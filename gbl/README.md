# Generic Bootloader Library

This directory hosts the Generic Bootloader Library project. A Bazel
workspace is setup for building the library as well as an EFI executable that
can be loaded directly from the firmware.

## Build

The GBL project are intended to be built from the
[Android Bootloader](https://source.android.com/docs/setup/create/cuttlefish-bootloader-dev#develop-bootloader)
super project checkout.

To build the EFI application:

```
./tools/bazel run //u-boot:gbl_efi_dist --extra_toolchains=@gbl//toolchain:all
```
The above builds the EFI application for all of `x86_64`, `x86_32`, `aarch64`
and `riscv64` platforms.

To run the set of unit tests:

```
./tools/bazel test @gbl//tests --extra_toolchains=@gbl//toolchain:all
```

## Run the EFI application

### Boot Android on Cuttlefish

If you have a main AOSP checkout and is setup to run
[Cuttlefish](https://source.android.com/docs/setup/create/cuttlefish), you can
run the EFI image directly with:

```
launch_cvd --android_efi_loader=<path to the EFI image> ...
```

The above uses the same setting as a normal `launch_cvd` run, except that
insted of booting Android directly, the emulator first hands off to the EFI
application, which will take over booting android.

Note: For x86 platform, use the EFI image built for `x86_32`.

### Boot Fuchsia on Vim3

Booting Fuchsia on a Vim3 development board is supported. To run the
application:

1. Complete all
[bootstrap steps](https://fuchsia.dev/fuchsia-src/development/hardware/khadas-vim3?hl=en)
to setup Vim3 as a Fuchsia device.
2. Reboot the device into fastboot mode.
3. Run fastboot command:
```
fastboot stage <path to the EFI binary> && fastboot oem run-staged-efi
```

### Run on standalone QEMU

If you want to test the EFI image directly on QEMU with your custom
configurations:

1. Install EDK, QEMU and u-boot prebuilts

   ```
   sudo apt-get install qemu-system ovmf u-boot-qemu
   ```

1. Depending on the target achitecture you want to run:

   For `x86_64`:
   ```
   mkdir -p /tmp/esp/EFI/BOOT && \
   cp <path to EFI image> /tmp/esp/EFI/BOOT/bootx64.efi && \
   qemu-system-x86_64 -nographic \
       -drive if=pflash,format=raw,readonly=on,file=/usr/share/OVMF/OVMF_CODE.fd \
       -drive format=raw,file=fat:rw:/tmp/esp
   ```

   For `aarch64`:
   ```
   mkdir -p /tmp/esp/EFI/BOOT && \
   cp <path to EFI image> /tmp/esp/EFI/BOOT/bootaa64.efi && \
   qemu-system-aarch64 -nographic -machine virt -m 1G -cpu cortex-a57 \
       -drive if=pflash,format=raw,readonly=on,file=/usr/share/AAVMF/AAVMF_CODE.fd \
       -drive format=raw,file=fat:rw:/tmp/esp
   ```

   For `riscv64`:
   ```
   mkdir -p /tmp/esp/EFI/BOOT && \
   cp <path to EFI image> /tmp/esp/EFI/BOOT/bootriscv64.efi && \
   qemu-system-riscv64 -nographic -machine virt -m 256M \
       -bios /usr/lib/u-boot/qemu-riscv64/u-boot.bin \
       -drive format=raw,file=fat:rw:/tmp/esp,id=blk0 \
       -device virtio-blk-device,drive=blk0
   ```
