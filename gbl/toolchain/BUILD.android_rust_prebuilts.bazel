# Copyright (C) 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

load("@rules_rust//rust:defs.bzl", "rust_library", "rust_stdlib_filegroup")

package(default_visibility = ["//visibility:public"])

exports_files(["bin/*"])

filegroup(
    name = "binaries",
    srcs = glob([
        "bin/*",
        "lib/*",
        "lib64/*",
    ]),
)

# sysroot prebuilts for x86_64_unknown-linux-gnu
rust_stdlib_filegroup(
    name = "prebuilt_stdlibs",
    srcs = glob(["lib/rustlib/x86_64_unknown-linux-gnu/*"]),
)

# sysroot prebuilts for aarch64_unknown-uefi
rust_stdlib_filegroup(
    name = "aarch64-unknown-uefi_prebuilt_stdlibs",
    srcs = glob(["lib/rustlib/aarch64-unknown-uefi/lib/*"]),
)

# sysroot prebuilts for x86_64-unknown-uefi
rust_stdlib_filegroup(
    name = "x86_64-unknown-uefi_prebuilt_stdlibs",
    srcs = glob(["lib/rustlib/x86_64-unknown-uefi/lib/*"]),
)

# sysroot prebuilts for i686-unknown-uefi
rust_stdlib_filegroup(
    name = "i686-unknown-uefi_prebuilt_stdlibs",
    srcs = glob(["lib/rustlib/i686-unknown-uefi/lib/*"]),
)

rust_library(
    name = "liballoc",
    srcs = glob(["src/stdlibs/library/alloc/src/**/*.rs"]),
    compile_data = glob(["src/stdlibs/library/alloc/src/**/*.md"]),
    crate_name = "alloc",
    edition = "2021",
    deps = [
        "libcompiler_builtins",
        "libcore",
    ],
)

rust_library(
    name = "libcompiler_builtins",
    srcs = glob([
        "src/stdlibs/vendor/compiler_builtins/src/**/*.rs",
        "src/stdlibs/vendor/compiler_builtins/libm/src/**/*.rs",
    ]),
    compile_data = glob(["src/stdlibs/vendor/compiler_builtins/src/**/*.md"]),
    crate_features = [
        "compiler-builtins",
        "core",
        "default",
        "mem",
    ],
    crate_name = "compiler_builtins",
    edition = "2015",
    rustc_flags = ["--cap-lints=allow"],
    deps = ["libcore"],
)

rust_library(
    name = "libcore",
    srcs = glob([
        "src/stdlibs/library/core/src/**/*.rs",
        "src/stdlibs/library/stdarch/crates/core_arch/src/**/*.rs",
        "src/stdlibs/library/portable-simd/crates/core_simd/src/**/*.rs",
    ]),
    compile_data = glob([
        "src/stdlibs/library/core/src/**/*.md",
        "src/stdlibs/library/core/primitive_docs/*.md",
        "src/stdlibs/library/stdarch/crates/core_arch/src/**/*.md",
        "src/stdlibs/library/portable-simd/crates/core_simd/src/**/*.md",
    ]),
    crate_features = ["stdsimd"],
    crate_name = "core",
    edition = "2021",
    rustc_flags = ["--cap-lints=allow"],
)
